# Proyecto Gtk
Este programa es un viasulizador de archivos pdb. A partir de estos archivos genera y extrae información de la proteína seleccionada,
esta información es mostrada con su respectiva imagen.

# Comenzando
Este proyecto contiene una ventana principal, donde se presentan 3 botones, "abrir", "acerca de" y "visualzar", respectivamente.
Al presionar el botón abrir se ingresa a otra ventana que corresponde al directorio, esta tiene la finalidad de buscar el archivo pdb para su visualización,
la ventana directorio contiene 2 botones "Aceptar" y "Cancelar". Aceptar tiene por finalidad conseguir la ruta y generer la imagen e información del archivo pdb seleccionado
y cancelar destruye la ventana sin realizar ninguna acción. El botón acerca de abre un dialogo donde indica información y créditos del proyecto.
Por último el botón visualizar al hacer click debería mostrar que información se quiere mostrar del archivo seleccionado es decir "ATOM", "HETATM", "ANISOU" y "OTHERS".

# Pre-requisitos 

- Sistema operativo Linux
- Python3
- Biopandas
- Pymol

# Instalación

- Para instalar python3 debe ir al siguiente link y se elige la última versión:
  https://www.python.org/downloads/

- y luego se ejecuta lo siguiente en la terminal:
  tar xvf python-3.8.5.tgz cd Python-3.8.5 ./configure --prefix= make -j4 make install

- Para instalar Biopandas se ejucuta el siguiente comnado:
  sudo apt install python3-pip 
- y luego el siguiente:
  pip3 install biopandas

- para instalar pymol se debe realizar el suguiente comando en la temrinal:
  sudo apt-get install python3-pymol

# Consejos extra 
Para este proyecto se ocupa la librería Gtk donde se importa como si no la tiene debe instalarla con el siguiente codigo:
 sudo apt-get install python3-gi

# Para ejecutar
Se debe abrir la terminal e ingresar al directorio donde se guardó nuestro proyecto y luego ejecutar:
  python3 proyecto.py

# Construido con 

- Ubuntu: sistema operativo

- Python: lenguaje de programación

- Glade: aplicación para diseñar la gráfica

- Biopandas: librería para trabajar estructuras moleculares

- Pymol: Herramienta para crear la imagen de la molécula 

- Gtk:librería de interfaz gráfica
- Atom: editor de texto

# Versiones

- Ubuntu 20.04 LTS

- Python 3.8.2

- Glade 3.22.2

- Biopandas 0.2.5

- Pymol 2.4.0

- Gtk 3.24.18
- Atom 1.46.0

# Autores

- Catalina Méndez Cruz
- Martín Muñoz Vera 

# Expresiones de gratitud
Se agradece la infinita paciencia de las ayudantes del curso, también a los videos subidos de las clases y ejemplos del profesor Fabio Duráán Verdugo, profesor del curso Programación Avanzada.


