#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# importamos libreria
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

# clase para ventanas de dialogo: acerca de y visualizar
class dialogo():

    # contructor
    def __init__(self, title=""):
        # metodo cargar el archivo
        # objeto builder
        self.builder = Gtk.Builder()
        self.builder.add_from_file("ui/ui.glade")

        # atributo venta de dialogo "Visualizar"
        # definiendo un tamaño que se considera adecuado
        self.dialogo = self.builder.get_object("dlgVisualizar")
        self.dialogo.set_title("Vista")
        self.dialogo.resize(600, 400)

        # al clicker en visualizar, ventana de dialogo genra botones
        # aceptar y cancelar generando interaccion con el ususario
        botonAceptar = self.dialogo.add_button(Gtk.STOCK_OK,
                                                Gtk.ResponseType.OK)
        botonAceptar.set_always_show_image(True)
        botonAceptar.connect("clicked", self.botonAceptar_clicked)

        botonCancelar = self.dialogo.add_button(Gtk.STOCK_CANCEL,
                                                Gtk.ResponseType.CANCEL)
        botonCancelar.set_always_show_image(True)
        botonCancelar.connect("clicked", self.botonCancelar_clicked)

        # atributo ventana dialogo "acerca de"
        self.about = self.builder.get_object("dlgAbout")

    # metodo para boton aceptar, mostrando por consola la accion del usuario
    def botonAceptar_clicked(self, btn=None):
        print("El usuario acepto")

    # metodo para boton cancelar, mostrando por console la accion del usuario
    def botonCancelar_clicked(self, btn=None):
        self.dialogo.destroy()
        print("El usuario cerro")
