#!/usr/bin/env python
# -*- coding: utf-8 -*-

# se importan librerias y clases
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from dialogo import dialogo
from wnDirectorio import wnDirectorio
import os
import json
import pathlib


# Clase para ventana principal/inicio del visualizador
class principalWindow():

    # Constructor de la clase
    def __init__(self):
        # se define el objeto "builder"
        # se utiliza metodo para cargar el archivo
        self.builder = Gtk.Builder()
        self.builder.add_from_file("ui/ui.glade")

        # Atributo principal, se utiliza la maxima dimension de la ventana
        self.window = self.builder.get_object("window")
        self.window.set_title("Visualizador PDB")
        self.window.maximize()
        self.window.connect("destroy", Gtk.main_quit)

        # botones, ventana principal
        # generan la interaccion con el usuario
        boton_abrir = self.builder.get_object("btnAbrir")
        boton_abrir.connect("clicked", self.boton_Abrir_clicked)

        boton_acerca = self.builder.get_object("btnAcerca")
        boton_acerca.connect("clicked", self.boton_acerca_clicked)

        boton_visualizar = self.builder.get_object("btnVisualizar")
        boton_visualizar.connect("clicked", self.boton_visualizar_clicked)

        # label
        self.label_principal = self.builder.get_object("labelPrincipal")

        # atributo ventana dialogo para visualizar
        self.dialogo = self.builder.get_object("dlgVizualizar")

        # Liststore.
        self.liststore = self.builder.get_object("pdb_tree")
        self.model = Gtk.ListStore(*(1 * [str]))
        #self.liststore.set_model(model=self.model)

        # RendenText
        cell = Gtk.CellRendererText()

        column = Gtk.TreeViewColumn(title="Archivo(s) PDB",
                                    cell_renderer=cell,
                                    text=0)

        #self.liststore.append_column(column)

        #self.selection = self.liststore.get_selection()
        #self.selection.connect("changed", self.on_changed)

        # muestra en pantalla
        self.window.show_all()

    # Metodo para boton abrir: permite examinar el directorio
    # buscar archivos tipo .pdb
    def boton_Abrir_clicked(self, btn=None):
        abrir = wnDirectorio("dlgFileChooser")
        response = abrir.fileChooser.run()
        abrir.fileChooser.destroy()

    # Metodo para boton cerrar
    def boton_acerca_clicked(self, btn=None):
        acerca = dialogo(title="dlgAbout")
        response = acerca.about.run()
        acerca.about.destroy()

    # Metodo boton visualizar
    def boton_visualizar_clicked(self, btn=None):
        view = dialogo(title="dlgVisualizar")
        response = view.dialogo.run()
        view.resize(600, 400)
        view.dialogo.destroy()

# main, contiene iterador, carga los elementos de la interfaz gráfica
if __name__ == "__main__":
    WINDOW = principalWindow()
    Gtk.main()
