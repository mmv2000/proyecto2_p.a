#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# se importan las librerias
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import pymol
from biopandas.pdb import PandasPdb

# clase: ventana de directorio
class wnDirectorio():

    # contructor de clase
    def __init__(self, title=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("ui/ui.glade")

        # atributo escoger el directorio
        self.fileChooser = self.builder.get_object("dlgFileChooser")
        self.fileChooser.set_title("Directorio")
        self.fileChooser.show_all()

        # botones: acpetar para seleccionar un archivo
        # cancelar para regresar a la accion anterior
        botonAceptar = self.fileChooser.add_button(Gtk.STOCK_OK,
                                                Gtk.ResponseType.OK)
        botonAceptar.set_always_show_image(True)
        botonAceptar.connect("clicked", self.botonPDB)

        botonCancelar = self.fileChooser.add_button(Gtk.STOCK_CANCEL,
                                                Gtk.ResponseType.CANCEL)
        botonCancelar.set_always_show_image(True)
        botonCancelar.connect("clicked", self.botonCancelar_clicked)

    # metodo para obtener el archivo pdb selecionado
    # y obtener la informacion correspondiente
    def botonPDB(self, btn=None):
        self.archivo = self.fileChooser.get_file()
        self.nombreArchivo = self.archivo.get_basename()
        print(self.nombreArchivo)

        self.ruta = self.fileChooser.get_current_folder()
        print(self.ruta)

        # Información PDB
        info = []
        ppdb = PandasPdb()
        ppdb.read_pdb(self.ruta + "/" + self.nombreArchivo)
        for i in ppdb.df.keys():
            print (i)
            info.append(ppdb.df[i])#pasar
            print("\n")

            if (i != "ANISOU"): # PROBLEMA, se generan todos los archivos pero no el de ANISOU
                nom = self.nombreArchivo + "_" + i + ".pdb"
                ppdb.to_pdb(path = (self.ruta + "/" + nom),
                        records = [i],
                        gz = False,
                        append_newline = True)
        self.info = info

        # Imagen PDB
        pymol.cmd.load(self.ruta + "/" + self.nombreArchivo, self.nombreArchivo)
        pymol.cmd.disable("all")
        pymol.cmd.enable(self.nombreArchivo)
        pymol.cmd.load(self.ruta + "/" + self.nombreArchivo, self.nombreArchivo)
        pymol.cmd.disable("all")
        pymol.cmd.enable(self.nombreArchivo)
        print(pymol.cmd.get_names())
        pymol.cmd.hide("all")
        pymol.cmd.show("cartoon")
        pymol.cmd.set("ray_opaque_background", 0)
        pymol.cmd.pretty(self.nombreArchivo)
        pymol.cmd.png("%s.png" % (self.nombreArchivo))
        pymol.cmd.quit()

    # metodo para boton aceptar, mostrando por consola la accion del usuario
    def botonAceptar_clicked(self, btn=None):
        self.ruta_label.set_text('Ruta actual: %s' %(self.folder))
        self.clear_screen()
        print("El usuario acepto")

    # metodo para boton cancelar, mostrando por consola la accion del usuario
    def botonCancelar_clicked(self, btn=None):
        self.fileChooser.destroy()
        print("El usuario cerro")
